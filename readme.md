
**Project is builded by docker based on nginx, php 7.4 mysql 5.7, laravel 5.8**

## Document

- [Docker for window](https://docs.docker.com/docker-for-windows/)
- [Laravel framework](https://laravel.com/docs/5.8/configuration#introduction)


## Installation

- Install docker on computer
- cd root project's folder
- cd docker/
- run ```docker-composer up -d```
- run ```docker exec -it demo-php-fpm bash``` to into container app
- run ```composer install``` to setup project
- copy .env.example to .env

## Prepare database

- cd into container: ```docker exec -it demo-php-fpm bash```
- run database migration: ```php artisan migrate```
- run database seed: ```php artisan db:seed```
- **_If need refresh database run:_** ```php artisan migrate:fresh --seed```

## Links
#### Users:
- Login page: [http://localhost:8080/login](http://localhost:8080/login)
- Register page: [http://localhost:8080/register](http://localhost:8080/login)
- Home page: [http://localhost:8080/home](http://localhost:8080/login)
- User 's posts page: [http://localhost:8080/my-posts](http://localhost:8080/login)
- Publish posts page: [http://localhost:8080/publish-posts](http://localhost:8080/logi)
#### Admins:
- Login page: [http://localhost:8080/admin/login](http://localhost:8080/admin/login)
- Home page: [http://localhost:8080/admin/home](http://localhost:8080/admin/home)
- All of posts page: [http://localhost:8080/admin/all-posts](http://localhost:8080/admin/all-posts)
