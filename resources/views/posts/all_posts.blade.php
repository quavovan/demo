@extends('layouts.admin_app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <select id="filterByStatus">
                    <option value="1" <?php echo !isset($status) ? 'selected="selected"' : '' ?>>All</option>
                    <option value="2" <?php echo isset($status) && $status == 'private' ? 'selected="selected"' : '' ?>>Private</option>
                    <option value="3" <?php echo isset($status) && $status == 'publish' ? 'selected="selected"' : '' ?>>Publish</option>
                </select>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Updated at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr data-id="{{$post->id}}">
                            <th scope="row">
                                {{$post->id}}
                            </th>
                            <td>{{$post->title}}</td>
                            <td>{{$post->status}}</td>
                            <td>{{$post->created_at}}</td>
                            <td>{{$post->updated_at}}</td>
                            <td>
                                @if($post->status == 'private')
                                    <button type="button" class="btn btn-primary publishBtn">Publish</button>
                                @else
                                    <button type="button" class="btn btn-danger unPublishBtn">Unpublish</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
    <script src="{{asset('js/posts/list_all_posts.js')}}"></script>
@endsection
