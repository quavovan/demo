@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>{{ $data->title }}</h1>
                {!! $data->content !!}
            </div>
        </div>
    </div>
    <script src="{{asset('js/posts/add_edit.js')}}"></script>
@endsection
