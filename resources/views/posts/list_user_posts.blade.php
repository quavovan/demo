@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary addBtn">Add</button>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Updated at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr data-id="{{$post->id}}">
                            <th scope="row"><a href="{{route('detailPost', ['id' => $post->id])}}">{{$post->id}}</a>
                            </th>
                            <td>{{$post->title}}</td>
                            <td>{{$post->status}}</td>
                            <td>{{$post->created_at}}</td>
                            <td>{{$post->updated_at}}</td>
                            <td>
                                <button type="button" class="btn btn-primary editBtn">Edit</button>
                                <button type="button" class="btn btn-danger deleteBtn">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $posts->links() }}
            </div>
        </div>
    </div>
    <script src="{{asset('js/posts/list_user_posts.js')}}"></script>
@endsection
