@extends('layouts.admin_app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <ul>
                        <li><a href="{{route('allPosts')}}">All of posts</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
