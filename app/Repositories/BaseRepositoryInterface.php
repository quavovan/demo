<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function all();
    public function findById($id);
    public function save($data);
    public function updateById($id, $data);
    public function delete($id);
}
