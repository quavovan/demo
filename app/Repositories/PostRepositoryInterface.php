<?php

namespace App\Repositories;

interface PostRepositoryInterface extends BaseRepositoryInterface
{
    public function getDataByCreatedUser($userId);
    public function getPublishPost();
}
