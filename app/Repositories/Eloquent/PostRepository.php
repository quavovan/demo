<?php

namespace App\Repositories\Eloquent;

use App\Models\Post;
use App\Repositories\PostRepositoryInterface;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    protected $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    public function getDataByCreatedUser($userId, $paginateLimit = 10)
    {
        return $this->model->where('created_user', $userId)->paginate($paginateLimit);
    }

    public function getPublishPost($paginateLimit = 10)
    {
        return $this->model->where('status', 'publish')->paginate($paginateLimit);
    }
}
