<?php

namespace App\Repositories\Eloquent;

use App\Repositories\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param bool $paginate
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     * get all of data
     */
    public function all($condition = [], $paginate = true, $limit = 10)
    {
        $result = $this->model;
        if (isset($condition['status'])) {
            $result = $result->where('status', $condition['status']);
        }
        if ($paginate) {
            return $result->paginate($limit);
        }
        return $result->all();
    }

    /**
     * @param $data
     * @return mixed
     * create a new record
     */
    public function save($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @return mixed
     * get data by id
     */
    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * update data
     */
    public function updateById($id, $data)
    {
        return $this->model->where('id', $id)->update($data);
    }

    /**
     * @param $id
     * @return int
     * delete data by id
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
