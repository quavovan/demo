<?php

namespace App\Services;

use App\Repositories\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class PostService
{
    private $postRepository;
    private $status = ['private' => 'private', 'publish' => 'publish'];

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param $userId
     * @return mixed
     * get all of posts of a user
     */
    public function getPostsByUserId($userId)
    {
        return $this->postRepository->getDataByCreatedUser($userId);
    }

    /**
     * @return mixed
     * get all of publish posts
     */
    public function getPublishPost()
    {
        return $this->postRepository->getPublishPost();
    }

    /**
     * @param $data
     * @return mixed
     * create a post
     */
    public function create($data)
    {
        $data['created_user'] = Auth::id();
        $data['status'] = $this->status['private'];
        return $this->postRepository->save($data);
    }

    /**
     * @param $id
     * @return mixed
     * get a post by id
     */
    public function getPostById($id)
    {
        return $this->postRepository->findById($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     * update by id
     */
    public function update($id, $data)
    {
        return $this->postRepository->updateById($id, $data);
    }

    /**
     * @param $id
     * @return mixed
     * delete by id
     */
    public function deleteById($id)
    {
        return $this->postRepository->delete($id);
    }

    /**
     * @return mixed
     * get all of publish posts
     */
    public function getPublishPosts()
    {
        return $this->postRepository->getPublishPost();
    }

    public function getAllPosts($condition)
    {
        return $this->postRepository->all($condition);
    }
}
