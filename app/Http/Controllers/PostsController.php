<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    private $postService;
    private $postStatus = [
        'all', 'private', 'publish'
    ];

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    private function checkPostOfUserLogin($id)
    {
        $data = $this->postService->getPostById($id);
        if ($data->created_user != Auth::id()) {
            return false;
        }
        return true;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * list of user 's posts
     */
    public function listPostsOfUser()
    {
        $posts = $this->postService->getPostsByUserId(Auth::id());
        return view('posts.list_user_posts', ['posts' => $posts]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * show screen to create a post
     */
    public function showCreatePostForm()
    {
        $url = route('createPost');
        return view('posts.add_edit', compact('url'));
    }

    /**
     * @param CreatePostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * create a post
     */
    public function createPost(CreatePostRequest $request)
    {
        $postData = $request->all();
        $this->postService->create($postData);
        return redirect()->route('myPosts');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * show edit a post form
     */
    public function showEditPostForm($id)
    {
        $url = route('updatePost', ['id' => $id]);
        $data = $this->postService->getPostById($id);
        if ($data->created_user != Auth::id()) {
            abort(404);
        }
        return view('posts.add_edit', compact('url', 'data'));
    }

    /**
     * @param CreatePostRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * update a post
     */
    public function updatePost(CreatePostRequest $request, $id)
    {
        if (!$this->checkPostOfUserLogin($id)) {
            abort(404);
        }
        $postData = $request->all();
        unset($postData['_token']);
        $this->postService->update($id, $postData);
        return redirect()->route('myPosts');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * delete a post
     */
    public function delete(Request $request)
    {
        $id = $request->input('id');
        if (!$this->checkPostOfUserLogin($id)) {
            abort(404);
        }
        $this->postService->deleteById($id);
        return response()->json(['success' => true]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * view detail of a post
     */
    public function detail($id)
    {
        $data = $this->postService->getPostById($id);
        if ($data->status == 'publish' || $data->created_user == Auth::id()) {
            return view('posts.detail', compact('data'));
        }
        abort(404);

    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * list publish posts
     */
    public function listPublishPosts()
    {
        $posts = $this->postService->getPublishPost();
        return view('posts.list_publish_posts', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * view all of posts
     */
    public function getAll(Request $request)
    {
        $status = $request->query('status');
        $condition = [];
        if ($status) {
            $condition['status'] = $status;
        }
        $posts = $this->postService->getAllPosts($condition);
        return view('posts.all_posts', compact('posts', 'status'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * publish a post
     */
    public function publish(Request $request)
    {
        $id = $request->input('id');
        $dataUpdate = [
            'status' => 'publish'
        ];
        $this->postService->update($id, $dataUpdate);
        return response()->json(['success' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * unpublish a post
     */
    public function unpublish(Request $request)
    {
        $id = $request->input('id');
        $dataUpdate = [
            'status' => 'private'
        ];
        $this->postService->update($id, $dataUpdate);
        return response()->json(['success' => true]);
    }

}
