<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:100|unique:members,email,' . $this->id,
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'tel' => 'required|max:50',
            'nickname' => 'required|max:50|unique:members,nickname,' . $this->id
        ];
    }
}
