<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdminAuth\LoginController;
use App\Http\Controllers\PostsController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/admin/home', 'HomeController@adminHome')->name('adminHome')->middleware('auth:admin');
Route::get('/admin/login', [LoginController::class, 'showLoginForm'])->name('showAdminLogin');
Route::post('/admin/login', [LoginController::class, 'login'])->name('adminLogin');
Route::post('/admin/logout', [LoginController::class, 'logout'])->name('adminLogout');

Route::group(['middleware' => ['auth:admin'], 'prefix' => 'admin'], function () {
    Route::get('/home', 'HomeController@adminHome')->name('adminHome');
    Route::get('/all-posts', [PostsController::class, 'getAll'])->name('allPosts');
    Route::post('/post/publish', [PostsController::class, 'publish'])->name('publishPosts');
    Route::post('/post/unpublish', [PostsController::class, 'unpublish'])->name('unpublishPosts');
});

Route::group(['middleware' => 'auth'], function () {
    Route::post('/my-posts/create', [PostsController::class, 'createPost'])->name('createPost');
    Route::get('/my-posts/add', [PostsController::class, 'showCreatePostForm'])->name('showAddPostForm');
    Route::get('/my-posts/edit/{id}', [PostsController::class, 'showEditPostForm'])->name('showEditPostForm');
    Route::post('/my-posts/edit/{id}', [PostsController::class, 'updatePost'])->name('updatePost');
    Route::post('/my-posts/delete', [PostsController::class, 'delete'])->name('deletePost');
    Route::get('/my-posts/detail/{id}', [PostsController::class, 'detail'])->name('detailPost');
    Route::get('/my-posts', [PostsController::class, 'listPostsOfUser'])->name('myPosts');
    Route::get('/publish-posts', [PostsController::class, 'listPublishPosts'])->name('publishPosts');
});
