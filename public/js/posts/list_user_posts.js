let listUserPosts = function () {
    let handleDelete = function () {
        $(".deleteBtn").on('click', function () {
            let id = $(this).closest('tr').attr('data-id');
            let confirm = window.confirm("Are you sure?");
            if (confirm) {
                let url = baseUrl + '/my-posts/delete';
                var submitForm = $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        id: id
                    }
                });
                submitForm.done(function (data) {
                    if (data.success == true) {
                        window.location.href = baseUrl + '/my-posts';
                    }
                });
            }
        });
    }

    let clickEditBtn = function () {
        $('.editBtn').on('click', function () {
            let id = $(this).closest('tr').attr('data-id');
            window.location.href = baseUrl + '/my-posts/edit/' + id;
        });
    }

    let clickOnAddBtn = function () {
        $('.addBtn').on('click', function () {
            window.location.href = baseUrl + '/my-posts/add';
        });
    };


    return {
        init: function () {
            handleDelete();
            clickOnAddBtn();
            clickEditBtn();
        }
    }
}();

$(function () {
    listUserPosts.init();
});
