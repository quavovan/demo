let listAllPosts = function () {
    let clickPublishBtn = function () {
        $(".publishBtn").on('click', function () {
            let id = $(this).closest('tr').attr('data-id');
            let url = baseUrl + '/admin/post/publish';
            let submitForm = $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id
                }
            });
            submitForm.done(function (data) {
                if (data.success) {
                    window.location.href = baseUrl + '/admin/all-posts';
                }
            });
        })
    }

    let clickUnpublishBtn = function () {
        $(".unPublishBtn").on('click', function () {
            let id = $(this).closest('tr').attr('data-id');
            let url = baseUrl + '/admin/post/unpublish';
            let submitForm = $.ajax({
                type: "POST",
                url: url,
                data: {
                    id: id
                }
            });
            submitForm.done(function (data) {
                if (data.success) {
                    window.location.href = baseUrl + '/admin/all-posts';
                }
            });
        })
    }

    let changeSelectFilterStatus = function () {
        $('#filterByStatus').on('change', function () {
            const value = $(this).val();
            if (value == 1) {
                window.location.href = baseUrl + '/admin/all-posts';
            }
            if (value == 2) {
                window.location.href = baseUrl + '/admin/all-posts?status=private';
            }
            if (value == 3) {
                window.location.href = baseUrl + '/admin/all-posts?status=publish';
            }
        });
    }

    return {
        init: function () {
            clickPublishBtn();
            clickUnpublishBtn();
            changeSelectFilterStatus();
        }
    }
}();

$(function () {
    listAllPosts.init();
});
